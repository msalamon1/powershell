﻿<#
.NOTES
Author: Matt Salamon

.SYNOPSIS
Creates an HTML system report with information for the local fixed disk 
including size, freespace, and freespace percentage. 
This information is moudularized into a function so that additional 
information can later be added to the report with additional functions. 
The report can be run against one or more computers.
The report will generate one HTML file per computer. Specify the -Path with the 
directory path location for the HTML files to be written to.
Existing files WILL BE OVERWRITTEN.

.PARAMETER ComputerName
Computer name(s) of IP address(s). 

.PARAMETER Path
Path to the directory where the HTML will be written.
If the Path does not exist, it will be created
For each computer, an HTML file will be created and named with the corresponding computername

.EXAMPLE
.\new-HTMLreport
Report will be run against default parameters, which are:(-computername localhost,-Path C:\reports)

.EXAMPLE
.\new-HTMLreport -Computername Machine1, Machine2 -Path C:\Reports\ 
Generates 1 HTML file for each Machine. Files are written to the C:\reports directory. If the C:\reports directory does not exist, it will be created

.EXAMPLE 
"Machine1" | .\get-LocalDiskInfo.ps1 -path c:\reports\reports2\ -Verbose
Takes in the Computername as pipeline input. File is written to the C:\reports\reports2 directory. Verbose output is displayed on the console
#>


[CmdletBinding()]
param(
        [Parameter(Mandatory=$False,
                  ValueFromPipeline=$True,
                  ValueFromPipelineByPropertyName=$True)]
        [string[]]$Computername = 'localhost', 
        [Parameter(Mandatory=$False)]
        [string]$Path ="C:\reports"
        )

Process {

$style = @"
<style>
    body {
        color:#333333;
        font-family:Calibri,Tahoma;
        font-size: 16pt;
        }
 
    h1 {
        text-align:center;
        text-decoration:underline;
       }
    h2 {
        border-top:1px solid #808080 ;
        text-decoration:underline;
        }
    th {
        font-weight:bold;
        color:#FFFFFF;
        background-color:#006600;
        }
</style>
"@        

function Get-InfoLocalDisk {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$True)][string]$Computername
    )
$drives = Get-WmiObject -class Win32_LogicalDisk -Computername $computername -Filter "DriveType=3"
    foreach ($drive  in $drives) {
        $properties = [ordered]@{'Drive' = $drive.DeviceID;
                        'Local Disk Size(GB)' = "{0:N2}" -f ($drive.size / 1GB);
                        'Current FreeSpace(MB)' = "{0:N2}" -f ($drive.freespace / 1MB);
                        'FreeSpace Percentage (%)' = $drive.freespace / $drive.size * 100 -as[int]}
        $report =  [pscustomobject] $properties
        $report.PSTypeNames.Clear()
        $report.PSTypeNames.Add('LocalDiskReport')
        $report
        } # End foreach
    }#End function

foreach($computer in $Computername) {
    try {
        $good_connection = $True
        Write-Verbose "Connection check for $computer"
        Get-wmiObject -class Win32_Bios -ComputerName $computer -ErrorAction stop | Out-Null
        }
    catch {
        Write-Warning "connection to $computer could not be established. Please check connection and try again."
        $good_connection = $False
        }

    if($good_connection) {
        if(-not(test-path -PathType Container $path)) {
            Write-Verbose "$Path not found. Creating $Path now..."
            New-Item $Path -ItemType Container
            }
        $filepath = join-Path -Path $Path -childPath "$computer.html"
        $htmlDriveInfo = Get-InfoLocalDisk -Computername $Computer |
                         ConvertTo-HTML  -Fragment -PreContent "<h2> Local Fixed Disk Report</h2>" |
                         Out-String
        $parameters = @{'Head' = "<title>Disk Free Space Report for $Computer</title>$style";
                        'PreContent' = "<h1>System Report for $Computer</h1>"
                        'PostContent' =$htmlDriveInfo,"<hr/>",$(Get-Date)
                        }
        ConvertTo-HTML @parameters | Out-File -FilePath $filepath
        }#End if
    }#End foreach
    Write-Verbose "Script complete"
}#End process 
