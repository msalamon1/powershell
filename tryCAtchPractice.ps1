﻿<# 
   .Synopsis 
    This script will check for a report file and a failed machine report file and remove them both if they exist
    -this will ensure that fresh reports are created everytime the script is run. Next, it will take in Machine
    names from a .txt file and query wmi for information. Error Action on the WMI queries is set to "stop". 
    A try block is opened to attempt to get the wmi information from a server. If the server is contacted--- 
    there are no terminating errors-- the information is retrieved and stored in a report file. If, However a
    machine cannot be contacted, then becasue the EA is set to stop, a terminating error will be generated. 
    This error will be caught and control will pass to the Catch block-where the server names will be stored 
    in a failed report file.   
   
   .Notes
    Requirements: 
    Your organization is preparing to renew maintenance contracts on all of your server computers.
    You need to provide management with a report that details each server's computer name, manufacturer, model, number
    of logical processors, and installed physical memory. Most of the servers do not have Windows PowerShell installed.
    None of the servers a separated from your client computer by a firewall. The server computer names are provided to
    you in a file named C:\Servers.txt, which contains one computer name per line.
    
    Minimum Requirements: 
    Optional Criteria: 
    Suppress error messages if a computer cannot be contacted ---Note-this
    is handled by setting EA to stop-and catching the terminating errors in the catch block. This way-we can supress the
    errors , but handle them properly-rather than simply supressing and ignoring them.
    
    NAME: WinterGames2.ps1
    
    AUTHOR: Matt Salamon
    
    LASTEDIT: 02/11/2013 16:49:40 

   
#Requires -Version 3.0 
#> 

#$errorActionpreference = 'stop'
cls

$computers = Gc c:\tfs\files\scripts\powershell\computers.txt 
foreach ($arg in $computers) {
                        Try {
                         
                         GC d:\foo -EA 'stop'
                         "NICE $arg"
                         }
                         catch {
                            
                            "$arg is f'ed up"
                            $error[0].ToString() + $error[0].InvocationInfo.PositionMessage | write-Host -foregroundcolor "green"
                            
                            }
                            
                        }