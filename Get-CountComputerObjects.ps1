﻿<#
.Notes  
   Author: Matt Salamon
   Date; 10/12/2012
   Pupose: Query AD for a count of computer objects

.Synopsis
   function will take paramaters for OU
.DESCRIPTION
  This function will query AD and output a count of the objects for whatever OU 
  -or pth of OUs was given in the parameter list. 
  To run, dot source the script, run the funtion , and add paramter
.EXAMPLE
   get-OUCount
   default paramter is Ou= Cafe. This will output a count of the cafe machines
.EXAMPLE
   get-OuCount "get-OUCount "Ou=desktops, OU = Orlando"
   output:    516 Ou=desktops, OU = Orlando  Machines
#>


function get-OUCount
{
 [CmdletBinding()]
param (
[string] $ou = 'OU = cafe'
)
$Domain = ‘LDAP://' + $ou +',OU=Workstations,OU=Hardware OU,OU=KU Online,DC=Charlie,DC=kaplaninc,DC=com’
$Root = New-Object DirectoryServices.DirectoryEntry $Domain
$select = New-Object DirectoryServices.DirectorySearcher
$select.SearchRoot = $root
$adobj= $select.findall() 
return $adobj.count 

}